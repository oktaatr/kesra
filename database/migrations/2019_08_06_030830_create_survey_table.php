<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey', function (Blueprint $table) {
            $table->integer('id_survey')->autoIncrement();
            $table->string('nama_orang');
            $table->string('nik', 16);
            $table->date('tgl_lahir');
            $table->enum('jns_kelamin', ['laki', 'perempuan'])->default('perempuan');
            $table->enum('jns_pendidikan', ['sd', 'smp', 'sma', 'diploma', 'sarjana'])->default('smp');
            $table->integer('status_survey');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey');
    }
}
