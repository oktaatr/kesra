<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$role)
    {
        if (empty(session('activeUser'))) {
            return redirect('/login');
        }
        else{
            foreach ($role as $roles) {
                if ($request->session()->get('activeUser')->role == $roles) {
                    return $next($request);
                }
            }
            if ($request->session()->get('activeUser')->role != $roles) {
                return abort(503, 'Anda tidak memiliki hak akses');
            }
        }
    }
}
