<?php

namespace App\Http\Controllers;

use App\Models\Survei;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BackendController extends Controller
{
    public function index(Request $request){
        return view('backend.dashboard.index');
    }

    public function grafik(Request $request){
        $data=DB::table('survey')
            ->whereYear('tgl_lahir', '1985')
            ->get();
        $total=count($data);
        $params = [
            'data'=>$data,
        ];
        return response()->json($total);
    }
}
