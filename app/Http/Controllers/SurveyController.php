<?php

namespace App\Http\Controllers;

use App\Models\Survei;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SurveyController extends Controller
{
    public function indexModerator(){
        return view('backend.survey.index_moderator');
    }

    public function indexSurveyor(){
        return view('backend.survey.index');
    }

    public function indexCreate(){
        return view('backend.survey.index_create');
    }

    public function getSurveymoderator(){
        $survey = Survei::all();
        return DataTables::of($survey)
            ->addColumn('action', function ($survey) {
                if ($survey->status_survey==0){
                    return '<center><a onclick="verifData('.$survey->id_survey.')" style="color: #ffffff" class="btn btn-xs btn-success"><i class="fas fa-certificate"></i> Verifikasi</a></center>';
                }
                return '<center><a href="#" class="btn btn-xs btn-default"><i class="">-</i></a></center>';
            })
            ->addColumn('status', function ($survey) {
                if ($survey->status_survey==0){
                    return '<center><a href="#" class="btn btn-xs btn-danger"><i class="fas fa-check-circle">Belum Verifikasi</i></a></center>';
                }
                return '<center><a href="#" class="btn btn-xs btn-success"><i class="fas fa-check-circle">Terverifikasi</i></a></center>';
            })
            ->rawColumns(['status','action'])
            ->make(true);
    }

    public function getSurveyor(){
        $survey = Survei::all();
        return DataTables::of($survey)
            ->make(true);
    }

    public function verifSurvey(Request $request){
        $id=$request->id;
        $data=Survei::find($id);
        $data->status_survey=1;
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Verifikasi berhasil</div>
            <script> scrollToTop(); reload(1150); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Verifikasi gagal!</div>";
        }
    }

    public function saveSurvey(Request $request){
        $data=new Survei();
        $data->nama_orang=$request->data_nama;
        $data->nik=$request->data_nik;
        $data->tgl_lahir=$request->ttl;
        $data->jns_kelamin=$request->gender;
        $data->jns_pendidikan=$request->pendidikan;
        $data->status_survey=0;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data berhasil di simpan!</div>
            <script>scrollToTop(); redirect(650, '/surveyor-survey'); </script>";
        }catch (\Exception $e){
            return "
            <div class='alert alert-danger'>Gagal menyimpan data</div>
            <script> scrollToTop(); reload(1300); </script>";
        }
    }
}
