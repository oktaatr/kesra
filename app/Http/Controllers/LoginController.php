<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index(Request $request){
        if ($request->session()->exists('activeUser')){
            return redirect('/backend');
        }
        return view('login.index');
    }

    public function login(Request $request){
        $username=$request->username;
        $password=$request->password;

        $activeUser=User::where(['username'=>$username])->first();
        if (is_null($activeUser)){
            return "<div class='alert alert-danger'>Pengguna tidak ditemukan</div>";
        }else{
            if ($activeUser->password==sha1($password)){
                $request->session()->put('activeUser', $activeUser);
                return "
            <div class='alert alert-success'>Login berhasil!</div>
            <script> scrollToTop(); reload(1000); </script>";
            }else{
                return "<div class='alert alert-danger'>Password Anda salah</div>";
            }
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }
}
