<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Survei extends Model
{
    protected $table = 'survey';
    protected $primaryKey = 'id_survey';
    public $timestamps = false;

}
