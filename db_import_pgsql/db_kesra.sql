PGDMP                         w            db_kesra    10.7    11.3                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    392313    db_kesra    DATABASE     �   CREATE DATABASE db_kesra WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_Indonesia.1252' LC_CTYPE = 'English_Indonesia.1252';
    DROP DATABASE db_kesra;
             postgres    false            �            1259    392316 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE public.migrations;
       public         postgres    false            �            1259    392314    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public       postgres    false    197                       0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
            public       postgres    false    196            �            1259    392337    survey    TABLE       CREATE TABLE public.survey (
    id_survey integer NOT NULL,
    nama_orang character varying(255) NOT NULL,
    nik character varying(16) NOT NULL,
    tgl_lahir date NOT NULL,
    jns_kelamin character varying(255) DEFAULT 'perempuan'::character varying NOT NULL,
    jns_pendidikan character varying(255) DEFAULT 'smp'::character varying NOT NULL,
    status_survey integer NOT NULL,
    CONSTRAINT survey_jns_kelamin_check CHECK (((jns_kelamin)::text = ANY ((ARRAY['laki'::character varying, 'perempuan'::character varying])::text[]))),
    CONSTRAINT survey_jns_pendidikan_check CHECK (((jns_pendidikan)::text = ANY ((ARRAY['sd'::character varying, 'smp'::character varying, 'sma'::character varying, 'diploma'::character varying, 'sarjana'::character varying])::text[])))
);
    DROP TABLE public.survey;
       public         postgres    false            �            1259    392335    survey_id_survey_seq    SEQUENCE     �   CREATE SEQUENCE public.survey_id_survey_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.survey_id_survey_seq;
       public       postgres    false    201                       0    0    survey_id_survey_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.survey_id_survey_seq OWNED BY public.survey.id_survey;
            public       postgres    false    200            �            1259    392324    users    TABLE     �   CREATE TABLE public.users (
    id integer NOT NULL,
    nama character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    role integer NOT NULL
);
    DROP TABLE public.users;
       public         postgres    false            �            1259    392322    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    199                       0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    198            |
           2604    392319    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196    197            ~
           2604    392340    survey id_survey    DEFAULT     t   ALTER TABLE ONLY public.survey ALTER COLUMN id_survey SET DEFAULT nextval('public.survey_id_survey_seq'::regclass);
 ?   ALTER TABLE public.survey ALTER COLUMN id_survey DROP DEFAULT;
       public       postgres    false    201    200    201            }
           2604    392327    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199                      0    392316 
   migrations 
   TABLE DATA               :   COPY public.migrations (id, migration, batch) FROM stdin;
    public       postgres    false    197   .       	          0    392337    survey 
   TABLE DATA               s   COPY public.survey (id_survey, nama_orang, nik, tgl_lahir, jns_kelamin, jns_pendidikan, status_survey) FROM stdin;
    public       postgres    false    201   �                 0    392324    users 
   TABLE DATA               C   COPY public.users (id, nama, username, password, role) FROM stdin;
    public       postgres    false    199                     0    0    migrations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.migrations_id_seq', 2, true);
            public       postgres    false    196                       0    0    survey_id_survey_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.survey_id_survey_seq', 2, true);
            public       postgres    false    200                       0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 2, true);
            public       postgres    false    198            �
           2606    392321    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public         postgres    false    197            �
           2606    392349    survey survey_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.survey
    ADD CONSTRAINT survey_pkey PRIMARY KEY (id_survey);
 <   ALTER TABLE ONLY public.survey DROP CONSTRAINT survey_pkey;
       public         postgres    false    201            �
           2606    392332    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    199            �
           2606    392334    users users_username_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_unique UNIQUE (username);
 E   ALTER TABLE ONLY public.users DROP CONSTRAINT users_username_unique;
       public         postgres    false    199               K   x�=�A
� еs���v��c2�V��>j�[?�RA�ďխt�h捽����ŝ��F$�4c~�3籈�UNe      	   t   x�=�;�0 ��9E/d���0���b��4��=���?�c}�zvm���j�,��M���#rL#<|.��v��@!��k��}����~�,���}k���`�ڴ����c8mB��!�         t   x�%�A
1 �s�I�61G_��$�.�h��
�^��L��}s���=�OTDݖ�JQ��`�5ԩz't�Nuȉ�]�>������w6kԂ�[CDm���1��G^����R�$�     