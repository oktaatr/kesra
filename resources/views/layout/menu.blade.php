<li class="{{request()->is('backend')?'active':''}}"><a href="{{url('backend')}}"><i class="icon-display4 position-left"></i> Dashboard</a></li>

@if(Session::get('activeUser')->role==1)
    <li class="{{request()->is('moderator-survey')?'active':''}}"><a href="{{url('moderator-survey')}}"><i class="icon-database-menu position-left"></i> Data Survey</a></li>
@else
    <li class="{{request()->is('surveyor-survey')?'active':''}}"><a href="{{url('surveyor-survey')}}"><i class="icon-database-menu position-left"></i> Data Survey</a></li>
@endif

