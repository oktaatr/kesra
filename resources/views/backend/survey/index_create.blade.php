@extends('layout.main')

@section('content-title')
    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Data Survey</span> - Masyarakat</h4>

    <ul class="breadcrumb breadcrumb-caret position-right">
        <li><a href="index.html">Home</a></li>
        <li class="active">Tambah Survey</li>
    </ul>
@endsection
@section('content')
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Form horizontal -->
                <div class="panel panel-flat">
                    <div id="result-form-konten"></div>
                    <div class="panel-heading">
                        <h5 class="panel-title">Tambah Data Survey</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" onsubmit="return false;" id="form-konten">
                            <fieldset class="content-group">
                                <legend class="text-bold"></legend>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Nama Orang</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="data_nama">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">NIK</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="data_nik">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Tgl Lahir</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="date" name="ttl">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Jenis Kelamin</label>
                                    <div class="col-md-10">
                                        <label class="radio-inline">
                                            <input type="radio" value="laki" class="styled" name="gender">
                                            Laki-laki
                                        </label>

                                        <label class="radio-inline">
                                            <input type="radio" value="perempuan" class="styled" name="gender">
                                            Perempuan
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Riwayat Pendidikan</label>
                                    <div class="col-md-10">
                                        <select class="select" name="pendidikan">
                                            <option value="sd">SD</option>
                                            <option value="smp">SMP</option>
                                            <option value="sma">SMA</option>
                                            <option value="diploma">Diploma</option>
                                            <option value="sarjana">Sarjana</option>
                                        </select>
                                    </div>
                                </div>
                                <input type='hidden' name='_token' value='{{ csrf_token() }}'>

                            </fieldset>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /form horizontal -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/ui/drilldown.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/js/pages/form_layouts.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#form-konten').submit(function () {
                var data = getFormData('form-konten');
                ajaxTransfer('/surveyor-save', data, '#result-form-konten');
            })
        })

    </script>
@endsection
