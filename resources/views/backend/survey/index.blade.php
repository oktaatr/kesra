@extends('layout.main')
@section('content-title')
        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Data Survey</span> - Masyarakat</h4>

        <ul class="breadcrumb breadcrumb-caret position-right">
            <li><a href="index.html">Home</a></li>
            <li class="active">Data Survey</li>
        </ul>
@endsection
@section('content')
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page length options -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Data Survey</h5>
                        <div class="heading-elements">
                            <a href="{{url('surveyor-add')}}" style="float: left" class="btn-xs btn-primary">
                                <i class="fas fa-plus-circle"></i> Tambah Data
                            </a>
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <table class="table" id="table-data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIK</th>
                            <th>Tanggal Lahir</th>
                            <th>Jenis Kelamin</th>
                            <th>Jenis Pendidikan</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- /page length options -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
@endsection
@section('scripts')
    <script>
    $(document).ready(function () {
        var tableData = $('#table-data').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{url('surveyor-data')}}',
            columns: [
                {data: 'id_survey',},
                {data: 'nama_orang',},
                {data: 'nik',},
                {data: 'tgl_lahir',},
                {data: 'jns_kelamin',},
                {data: 'jns_pendidikan',},
            ],
            pageLength: 10,
        });
    });
</script>
@endsection
