@extends('layout.main')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css">
@endsection
@section('content-title')
    <h4>
        <i class="icon-arrow-left52 position-left"></i>
        <span class="text-semibold">Home</span> - Dashboard
        <small class="display-block">Good morning, {{Session::get('activeUser')->nama}}</small>
    </h4>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Persebaran Masyarakat</h6>
                    <div class="heading-elements">
                        <form class="heading-form" action="#">
                            <div class="form-group">

                            </div>
                        </form>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12" >
                            <div class="col-md-4">
                                <label>Tahun Awal</label>
                                <input placeholder="Masukkan tahun awal" class="form-control" type="text" id="datepicker" />
{{--                                <input placeholder="Masukkan tahun tujuan" class="form-control" type="text" id="datepicker2" />--}}
                            </div>
                            <div class="col-md-4">
                                <label>Tahun Tujuan</label>
{{--                                <input placeholder="Masukkan tahun awal" class="form-control" type="text" id="datepicker" />--}}
                                <input placeholder="Masukkan tahun tujuan" class="form-control" type="text" id="datepicker2" />
                            </div>

                        </div>
                        <div class="col-md-12" style="margin-top: 60px">
                            <canvas id="grafik"></canvas>
                        </div>

                    </div>
                </div>

                <div class="position-relative" id="traffic-sources"></div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script>
        var ctx = document.getElementById("grafik").getContext('2d');
        var myChart = new Chart(ctx,{
            type: "bar",
            data: {
                labels: "",
                datasets: ""
            },
            options: {
                stepSize: 1000
            }
        });

        $("#datepicker").datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years"
        });
        $("#datepicker2").datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years"
        });
        $.ajax({
            type: 'GET',
            url: '',
            dataType: 'json',
            success: function (data) {
                console.log('success')
                var label = []
                var datasd = []
                var datasmp = []
                var datasma = []
                var datadiploma = []
                var container = []
                // for(var x =0;x<data.length;x++){
                //     label.push(data[x].daerah)
                //     keluaran.push(data[x].hasil)
                //     target_prediksi.push(data[x].prediksi)
                // }
                //
                // container.push({
                //     label: "Target",
                //     data: keluaran,
                //     backgroundColor: 'rgb(8,72,161)',
                //     borderColor: 'rgb(8,72,161)'
                // })
                //
                // container.push({
                //     label: "# prediksi",
                //     data: target_prediksi,
                //     backgroundColor: 'rgb(122,158,255)',
                //     borderColor: 'rgb(122,158,255)'
                // })
                //
                // bikinChart("grafik",label,container)
            }
        });
        function bikinChart(canvasId, label, datasets){
            myChart.data.labels = label;
            myChart.data.datasets = datasets;
            myChart.update();
        }
    </script>
@endsection
