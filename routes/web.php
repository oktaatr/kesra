<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//1 role moderator
//2 role surveyor

Route::get('login', 'LoginController@index');
Route::get('logout', 'LoginController@logout');
Route::post('validate-login','LoginController@login');
Route::group(['middleware' => 'status-role:1,2'], function () {
    Route::get('/', 'LoginController@index');
    Route::get('backend', 'BackendController@index');
    Route::get('grafik', 'BackendController@grafik');
});

Route::group(['middleware' => 'status-role:1'], function () {
    Route::get('moderator-survey', 'SurveyController@indexModerator');
    Route::get('moderator-data', 'SurveyController@getSurveymoderator');
    Route::post('moderator-verif','SurveyController@verifSurvey');
});

Route::group(['middleware' => 'status-role:2'], function () {
    Route::get('surveyor-survey', 'SurveyController@indexSurveyor');
    Route::get('surveyor-data', 'SurveyController@getSurveyor');
    Route::get('surveyor-add','SurveyController@indexCreate');
    Route::post('surveyor-save','SurveyController@saveSurvey');
});

